package com.sapient.activitycycle;


import android.arch.persistence.room.Room;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.sapient.sqlite.AppDatabase;
import com.sapient.sqlite.RoomDBRunnable;
import com.sapient.sqlite.UserDetailsDAO;
import com.sapient.sqlite.UserDetailsDBHelper;
import com.sapient.sqlite.UserDetailsEntry;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;


public class BlankFragment extends Fragment {

    EditText nameEdit;
    EditText companyEdit;
    EditText empIDEdit;
    RadioGroup rgrp;
    RadioButton radioButton;
    BlankFragment2 bf2;
    View view;
    AppDatabase db;

    private Handler uiHandler = new Handler();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.e("BlankFrangment " , "  onCreateView method called");
        view = inflater.inflate(R.layout.fragment_blank,container,false);
        nameEdit  =  view.findViewById(R.id.nameEdit1);
        companyEdit = view.findViewById(R.id.companyEdit2);
        empIDEdit =  view.findViewById(R.id.EmpIDEdit3);
        rgrp = view.findViewById(R.id.radioGrp);
        db = Room.databaseBuilder( getActivity().getApplicationContext(),
                AppDatabase.class, "user_details_db").build();


        if(getArguments() != null){
            if(getArguments().get("mode").equals("SP")){
                Log.e("Mode - " , " Shared Preference Mode selected");
                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        "com.sapient.activitycycle.PREFERENCE_FILE" , Context.MODE_PRIVATE);
                String name = sharedPref.getString("name" , "NoName");
                String company = sharedPref.getString("company" , "NoCompany");
                String empId = sharedPref.getString("empId" , "NoEmpId");
                String gender = sharedPref.getString("gender" , "NoGender");
                nameEdit.setText(name);
                companyEdit.setText(company);
                empIDEdit.setText(empId);
                if(gender.equals("Male"))
                    rgrp.check(R.id.radioBtn1);
                else
                    rgrp.check(R.id.radioBtn2);


            }
            if(getArguments().get("mode").equals("SQLite")){
                Log.e("Mode - " , " SQLite Mode selected");
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            final UserDetailsEntry user = db.userDetailsDAO().getAll();
                            uiHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    nameEdit.setText(user.name);
                                    companyEdit.setText(user.company);
                                    empIDEdit.setText(user.empID);
                                    if(user.gender.equals("Male"))
                                        rgrp.check(R.id.radioBtn1);
                                    else
                                        rgrp.check(R.id.radioBtn2);

//                                    Log.e("userDetailsfromRoom :- " , user.name +" -  "+ user.empID +" -  "+ user.company +" -  " + user.gender);

                                }
                            });
                        }
                    }).start();



            }
        }else {
            Log.e("Mode - " , " NULLLLL");
        }

        view.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                radioButton = view.findViewById(rgrp.getCheckedRadioButtonId());
                if(radioButton!= null) {
                    Bundle args = new Bundle();
                    bf2 = new BlankFragment2();
                    args.putString("name", nameEdit.getText().toString());
                    args.putString("company", companyEdit.getText().toString());
                    args.putString("empId", empIDEdit.getText().toString());
                    args.putString("gender", radioButton.getText().toString());
                    bf2.setArguments(args);

                   saveToSharedPreferences();
                    //saveTOSQLLIte();
                    saveToRoomDatabase();

                    FragmentTransaction fr = getFragmentManager().beginTransaction();
                    fr.replace(R.id.fragment1, bf2);
                    fr.commit();
                }
            }
        });

        return view;
    }

    public void saveToRoomDatabase(){

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Log.e("BlankFrangment " , " Saving to Room Database");
                UserDetailsEntry user;
                user= db.userDetailsDAO().getAll();
                if(user != null){
                    db.userDetailsDAO().delete(user);

                }
                user=new UserDetailsEntry();
                user.empID =  empIDEdit.getText().toString();
                user.name =  nameEdit.getText().toString();
                user.company =  companyEdit.getText().toString();
                user.gender =  radioButton.getText().toString();
                db.userDetailsDAO().insertAll(user);

            }
        });


    }
    public void saveTOSQLLIte(){

        UserDetailsDBHelper dbHelper = new UserDetailsDBHelper(getContext());

        ContentValues values = new ContentValues();
        values.put(dbHelper.userEntry.COLUMN_NAME_USERNAME, nameEdit.getText().toString());
        values.put(dbHelper.userEntry.COLUMN_NAME_COMPANY, companyEdit.getText().toString());
        values.put(dbHelper.userEntry.COLUMN_NAME_EMPID, empIDEdit.getText().toString());
        values.put(dbHelper.userEntry.COLUMN_NAME_GENDER, radioButton.getText().toString());

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long newRowId = db.insert(dbHelper.userEntry.TABLE_NAME, null, values);
        Log.e("values entered in DB", " ID - " + newRowId);
    }
    public void saveToSharedPreferences(){
        Log.e("BlankFrangment " , " Saving to Shared Preferences");
        SharedPreferences sharedPref = getActivity().getSharedPreferences(
               "com.sapient.activitycycle.PREFERENCE_FILE" , Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("name" ,  nameEdit.getText().toString());
        editor.putString("company" , companyEdit.getText().toString());
        editor.putString("empId" ,  empIDEdit.getText().toString());
        editor.putString("gender" ,  radioButton.getText().toString());
        editor.apply();

        //Log.e("from shared pref :- " , sharedPref.getString("name","No value found"));
    }


 }
