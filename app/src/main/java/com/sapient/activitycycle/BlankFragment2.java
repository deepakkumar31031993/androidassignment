package com.sapient.activitycycle;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class BlankFragment2 extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_blank_fragment2,container,false);

         // getting information from bundle object
        String name = getArguments().getString("name");
        String company = getArguments().getString("company");
        String empId = getArguments().getString("empId");
        String gender = getArguments().getString("gender");

        TextView nameView = view.findViewById(R.id.nameEdit1);
        nameView.setText(name);
        TextView companyView = view.findViewById(R.id.companyEdit2);
        companyView.setText(company);
        TextView empIdView = view.findViewById(R.id.EmpIDEdit3);
        empIdView.setText(empId);
        TextView genderView = view.findViewById(R.id.GenderEdit4);
        genderView.setText(gender);
        view.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

         AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
          builder.setTitle("Mode");
          builder.setItems(R.array.mode_selector_array, new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                  Bundle args = new Bundle();
                  Log.e("Which - " , "  Mode selected " + which);
                  if(which == 0)
                      args.putString("mode","SP");
                  else
                      args.putString("mode","SQLite");

                    BlankFragment bf = new BlankFragment();
                    bf.setArguments(args);
                  FragmentTransaction fr = getFragmentManager().beginTransaction();
                  fr.replace(R.id.fragment1,bf);
                  fr.commit();
              }
          });

          builder.show();
            }
        });
        return  view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Inflate the layout for this fragment
    }
}
