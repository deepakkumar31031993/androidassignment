package com.sapient.activitycycle;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sapient.http.BookData;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.MyViewHolder> {

    private List<BookData> bookData;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView item,bookID, bookJournal ,bookEiSSN, bookPublicationDate,bookArticleType,bookAuthorDisplay;

        public MyViewHolder(View view) {
            super(view);
            item = view.findViewById(R.id.itemCount);
            bookID = view.findViewById(R.id.bookid);
            bookJournal = view.findViewById(R.id.journal);
            bookEiSSN = view.findViewById(R.id.eiSSN);
            bookPublicationDate = view.findViewById(R.id.publicationDate);
            bookArticleType = view.findViewById(R.id.articleType);
            bookAuthorDisplay = view.findViewById(R.id.authors);

        }
    }

    public BookAdapter(List<BookData> bookData) {
        this.bookData = bookData;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.book_list_view, parent, false);

        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BookData book = bookData.get(position);
        holder.item.setText("Item :- " + ( position +1 ));
        holder.bookID.setText("ID  :- " +book.getId());
        holder.bookJournal.setText("Journal :- " + book.getJournal());
        holder.bookEiSSN.setText("EiSSN :- "+ book.getEissn());
        holder.bookArticleType.setText("Article Type :- "+ book.getArticleType());
        holder.bookPublicationDate.setText("Publication Date :- "+ book.getPublicationDate());
        StringBuilder sb = new StringBuilder();
        for(String str : book.getAuthorDisplay()){
                sb.append("                   "+ str + "\n");
        }
        holder.bookAuthorDisplay.setText("Author Display :- \n"+ sb.toString());

    }

    @Override
    public int getItemCount() {
        return bookData.size();
    }
}
