package com.sapient.activitycycle;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.gson.Gson;
import com.sapient.activitycycle.BookAdapter;
import com.sapient.activitycycle.BookDetails;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import com.sapient.http.BookData;
import com.sapient.http.BookDataJsonParser;
import com.sapient.http.HttpService;
import com.sapient.mvvm.model.DNAResponse;
import com.sapient.mvvm.model.DocsModel;
import com.sapient.mvvm.model.ResponseModel;
import com.sapient.mvvm.retrofit.WebService;
import com.sapient.mvvm.viewmodel.DocsViewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BookDetails extends AppCompatActivity {

    RecyclerView recyclerView ;
    BookAdapter bookAdapter;
    DocsViewModel docsViewModel;
    List<BookData> data = new ArrayList<>();
    public static Context context ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_book_details);
        context = getApplicationContext();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = findViewById(R.id.recycler_view);
        bookAdapter = new BookAdapter(data);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(bookAdapter);
        //String url = "http://api.plos.org/search?q=title:DNA";
        // new JSONAsyncTask().execute(url);

        docsViewModel = ViewModelProviders.of(this).get(DocsViewModel.class);
       
        docsViewModel.getDocs().observe(this, new Observer<DNAResponse>() {
            @Override
            public void onChanged(@Nullable DNAResponse dnaResponse) {
                Log.e("OnChanged " , "dnaResponse changedd");
                if(dnaResponse != null){
                    ResponseModel responseModel = dnaResponse.getmResponseModel();
                    List<DocsModel> docsModel = responseModel.getDocs();
                    BookData bookData ;
                    for(DocsModel docModel : docsModel){
                        bookData = new BookData();
                        bookData.setId(docModel.getId());
                        bookData.setEissn(docModel.getEissn());
                        bookData.setArticleType(docModel.getArticleType());
                        bookData.setJournal(docModel.getJournal());
                        //docModel.getAuthorDisplay();
                        bookData.setPublicationDate(docModel.getPublicationDate());

                        data.add(bookData);
                    }

                    bookAdapter.notifyDataSetChanged();

                }
            }
        });


    }

    private class JSONAsyncTask extends AsyncTask<String,Void, String> {

        HttpService service;
        BookDataJsonParser jsonParser;


        @Override
        protected String doInBackground(String... strings) {
            service= new HttpService();
            return  service.makeServiceCall(strings[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            // Log.e("json from url", s);

            jsonParser = new BookDataJsonParser(s);
            List<BookData> data = jsonParser.parseBookJson();
           /* for(BookData book : data){
                Log.e(" book  " , book.toString());
            }*/
            bookAdapter = new BookAdapter(data);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(bookAdapter);


        }
    }


}
