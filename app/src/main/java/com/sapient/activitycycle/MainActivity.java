package com.sapient.activitycycle;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.util.Log;

import com.google.gson.Gson;
import com.sapient.mvvm.model.DNAResponse;
import com.sapient.mvvm.model.DocsModel;
import com.sapient.mvvm.retrofit.WebService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    public Button clickMe;
    public static String LOG_TAG="MainActivity";
    private FragmentTransaction fragmentTransaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://api.plos.org/")
                            .addConverterFactory(GsonConverterFactory.create(new Gson()))
                            .build();
        WebService service = retrofit.create(WebService.class);
        service.getDocs().enqueue(new Callback<DNAResponse>() {
            @Override
            public void onResponse(Call<DNAResponse> call, Response<DNAResponse> response) {
                    Log.e("response ", response.body().getmResponseModel().getNumFound());
            }

            @Override
            public void onFailure(Call<DNAResponse> call, Throwable t) {

                Log.e("Failure " , "Failure");
            }
        });

        Log.i(LOG_TAG," -- onCreate-- ");
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment1,new BlankFragment());
        fragmentTransaction.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(LOG_TAG," -- onStart- ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(LOG_TAG," -- onResume- ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(LOG_TAG," -- onRestart- ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(LOG_TAG," -- onPause- ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(LOG_TAG," -- onStop- ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(LOG_TAG," -- onDestroy- ");
    }

   /* public void goNext(View view) {
        Intent intent = new Intent(this,SecondActivity.class);
        startActivity(intent);
        Log.i(LOG_TAG," -- goNext- ");
    }*/
}
