package com.sapient.activitycycle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class SecondActivity extends AppCompatActivity {

    public static String LOG_TAG="SecondActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Log.i(LOG_TAG," -- onCreate- ");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(LOG_TAG," -- onStart- ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(LOG_TAG," -- onResume- ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(LOG_TAG," -- onRestart- ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(LOG_TAG," -- onPause- ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(LOG_TAG," -- onStop- ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(LOG_TAG," -- onDestroy- ");
    }
    public void goBack(View view) {

        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);

        Log.i(LOG_TAG," -- goBack- ");
    }
}
