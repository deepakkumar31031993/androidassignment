package com.sapient.http;

import com.sapient.mvvm.model.DocsModel;

import java.util.ArrayList;
import java.util.List;

public class BookData extends DocsModel {
/*

    private String id;
    private String journal;
    private String eissn;
    private String publicationDate;
    private String articleType;
    private List<String> authorDisplay = new ArrayList<String>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJournal() {
        return journal;
    }

    public void setJournal(String journal) {
        this.journal = journal;
    }

    public String getEissn() {
        return eissn;
    }

    public void setEissn(String eissn) {
        this.eissn = eissn;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getArticleType() {
        return articleType;
    }

    public void setArticleType(String articleType) {
        this.articleType = articleType;
    }

    public List<String> getAuthorDisplay() {
        return authorDisplay;
    }

    public void setAuthorDisplay(List<String> authorDisplay) {
        this.authorDisplay = authorDisplay;
    }
*/

    @Override
    public String toString() {
        String book = "id = " + this.getId() + " article type" + this.getArticleType() +" journal "
                    + this.getJournal() + " Eissn " + this.getEissn() + " Date" +
                        this.getPublicationDate();

        return book;
    }
}
