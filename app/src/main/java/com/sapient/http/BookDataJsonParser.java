package com.sapient.http;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BookDataJsonParser {
    JSONObject jsonObject;
    JSONArray docsArrsay;
    public BookDataJsonParser(String jsonString){
        try{
            jsonObject = new JSONObject(jsonString);
            jsonObject = jsonObject.getJSONObject("response");
            docsArrsay = jsonObject.getJSONArray("docs");
        }catch(JSONException e){
            Log.e("json Parser" , "Error while parsing ");
        }

    }

    public List<BookData> parseBookJson(){
            List<BookData> list = new ArrayList<BookData>();
            try{

                for(int i =0; i < docsArrsay.length(); i++){
                    jsonObject = docsArrsay.getJSONObject(i);
                    BookData book = new BookData();
                    book.setId(jsonObject.getString("id"));
                    book.setJournal(jsonObject.getString("journal"));
                    book.setEissn(jsonObject.getString("eissn"));
                    book.setPublicationDate(jsonObject.getString("publication_date"));
                    book.setArticleType(jsonObject.getString("article_type"));
                    JSONArray authorArray = jsonObject.getJSONArray("author_display");
                    List<String> authorList = new ArrayList<String>();
                    for(int j = 0 ; j < authorArray.length() ; j++){
                        authorList.add(authorArray.get(j).toString());
                    }
                    book.setAuthorDisplay(authorList);
                    list.add(book);
                }
            }catch(JSONException e){
                Log.e("json Parser" , "Error while parsing ");
            }

            return list;
        }




}
