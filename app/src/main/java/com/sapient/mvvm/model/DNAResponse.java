package com.sapient.mvvm.model;

import com.google.gson.annotations.SerializedName;

public class DNAResponse {

    @SerializedName("response")
    private ResponseModel mResponseModel;

    public ResponseModel getmResponseModel() {
        return mResponseModel;
    }

    public void setmResponseModel(ResponseModel mResponseModel) {
        this.mResponseModel = mResponseModel;
    }
}
