package com.sapient.mvvm.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DocsModel {


    private String id;
    private String journal;
    private String eissn;
    @SerializedName("publication_date")
    private String publicationDate;
    @SerializedName("article_type")
    private String articleType;
    @SerializedName("author_display")
    private List<String> authorDisplay = new ArrayList<String>();
    @SerializedName("abstract")
    private List<String> abstractList = new ArrayList<String>();
    private String title_display;
    private String score;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJournal() {
        return journal;
    }

    public void setJournal(String journal) {
        this.journal = journal;
    }

    public String getEissn() {
        return eissn;
    }

    public void setEissn(String eissn) {
        this.eissn = eissn;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getArticleType() {
        return articleType;
    }

    public void setArticleType(String articleType) {
        this.articleType = articleType;
    }

    public List<String> getAuthorDisplay() {
        return authorDisplay;
    }

    public void setAuthorDisplay(List<String> authorDisplay) {
        this.authorDisplay = authorDisplay;
    }

    public List<String> getAbstractList() {
        return abstractList;
    }

    public void setAbstractList(List<String> abstractList) {
        this.abstractList = abstractList;
    }

    public String getTitle_display() {
        return title_display;
    }

    public void setTitle_display(String title_display) {
        this.title_display = title_display;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }



}
