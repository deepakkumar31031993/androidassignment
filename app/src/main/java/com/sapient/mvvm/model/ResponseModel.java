package com.sapient.mvvm.model;

import java.util.List;

public class ResponseModel {

    private String numFound;
    private String start;
    private String maxScore;
    private List<DocsModel> docs;

    public String getNumFound() {
        return numFound;
    }

    public void setNumFound(String numFound) {
        this.numFound = numFound;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(String maxScore) {
        this.maxScore = maxScore;
    }

    public List<DocsModel> getDocs() {
        return docs;
    }

    public void setDocs(List<DocsModel> docs) {
        this.docs = docs;
    }
}
