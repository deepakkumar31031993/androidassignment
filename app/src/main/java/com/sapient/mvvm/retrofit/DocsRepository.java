package com.sapient.mvvm.retrofit;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.google.gson.Gson;
import com.sapient.activitycycle.BookDetails;
import com.sapient.http.HttpService;
import com.sapient.mvvm.model.DNAResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DocsRepository {

    private WebService webService;

   public LiveData<DNAResponse> getDocs(){
       final MutableLiveData<DNAResponse> data = new MutableLiveData<>();

       /*Retrofit retrofit = new Retrofit.Builder()
               .baseUrl("http://api.plos.org/")
               .addConverterFactory(GsonConverterFactory.create(new Gson()))
               .build();
       webService = retrofit.create(WebService.class);
*/

       webService = HttpService.setUpRetroFitAndOkHttp2(BookDetails.context);
       webService.getDocs().enqueue(new Callback<DNAResponse>() {
            @Override
            public void onResponse(Call<DNAResponse> call, Response<DNAResponse> response) {
                Log.e("DocsRspository  " ,"Got response from webservice --- " + response.headers().get("Cache-Control"));

                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<DNAResponse> call, Throwable t) {

            }
        });

        return data;
    }
}
