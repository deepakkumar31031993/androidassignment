package com.sapient.mvvm.retrofit;

import com.sapient.mvvm.model.DNAResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface WebService {


    @GET("search?q=title:DNA")
    Call<DNAResponse> getDocs();
}
