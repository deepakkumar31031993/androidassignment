package com.sapient.mvvm.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import com.sapient.mvvm.model.DNAResponse;
import com.sapient.mvvm.retrofit.DocsRepository;

public class DocsViewModel extends ViewModel {

 private LiveData<DNAResponse> responseModel;
 private DocsRepository docsRepository;

    public DocsViewModel(){
        this.docsRepository = new DocsRepository();
    }


    public LiveData<DNAResponse> getDocs(){

        if(this.responseModel != null)
            return this.responseModel;

        this.responseModel = docsRepository.getDocs();
        return this.responseModel;
    }
}
