package com.sapient.sqlite;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {UserDetailsEntry.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract UserDetailsDAO userDetailsDAO();

}
