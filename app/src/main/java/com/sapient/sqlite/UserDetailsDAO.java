package com.sapient.sqlite;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface UserDetailsDAO {


    @Insert
    void  insertAll(UserDetailsEntry... user);

    @Query("SELECT * FROM UserDetailsEntry LIMIT 1")
    UserDetailsEntry getAll();

    @Delete
    void delete(UserDetailsEntry user);


}
