package com.sapient.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class UserDetailsDBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "UserDetailsReader.db";
    public static UserDetailsReaderContract.UserEntry userEntry;

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + userEntry.TABLE_NAME + " (" +
                    userEntry._ID + " INTEGER PRIMARY KEY," +
                    userEntry.COLUMN_NAME_USERNAME + " TEXT," +
                    userEntry.COLUMN_NAME_COMPANY + " TEXT," +
                    userEntry.COLUMN_NAME_EMPID + " TEXT," +
                    userEntry.COLUMN_NAME_GENDER + " TEXT)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + userEntry.TABLE_NAME;


    public UserDetailsDBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

}
