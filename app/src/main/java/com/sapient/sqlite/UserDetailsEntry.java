package com.sapient.sqlite;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class UserDetailsEntry {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "Employee_ID")
    public String empID;

    @ColumnInfo(name = "Employee_Name")
    public String name;
    @ColumnInfo(name = "Employee_CompanyName")
    public String company;
    @ColumnInfo(name = "Employee_Genders")
    public String gender;

}
