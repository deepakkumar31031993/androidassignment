package com.sapient.sqlite;

import android.provider.BaseColumns;

public class UserDetailsReaderContract {

    private UserDetailsReaderContract(){}

    public static class UserEntry implements BaseColumns {
        public static final String TABLE_NAME = "userdetails";
        public static final String COLUMN_NAME_USERNAME = "name";
        public static final String COLUMN_NAME_COMPANY = "company";
        public static final String COLUMN_NAME_EMPID = "empid";
        public static final String COLUMN_NAME_GENDER = "gender";
    }
}
